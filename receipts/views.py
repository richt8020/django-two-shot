from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, CreateAccountForm
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic.list import ListView


# class ReceiptListView(ListView):
#   model = Receipt
#   context_object_name = "receipts"
#   template_name = "receipts/list.html"


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receipts"
    ordering = ["-date"]
    paginate_by = 10

    def get_queryset(self):
        return super().get_queryset().filter(purchaser=self.request.user)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    return render(request, "receipts/create_receipt.html", {"form": form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    return render(request, "receipts/create_category.html", {"form": form})


@login_required
def create_account_view(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST, owner=request.user)
        if form.is_valid():
            form.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm(owner=request.user)
    return render(request, "receipts/create_account.html", {"form": form})
