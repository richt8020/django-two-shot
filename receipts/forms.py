from django import forms
from .models import Receipt, ExpenseCategory, Account


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class CreateAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]

    def __init__(self, *args, **kwargs):
        self.owner = kwargs.pop("owner", None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        account = super().save(commit=False)
        account.owner = self.owner
        if commit:
            account.save()
        return account
