from django.urls import path
from .views import ReceiptListView, create_receipt
from .views import (
    ReceiptListView,
    category_list,
    account_list,
    create_category,
    create_account_view,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("<int:purchaser>/", ReceiptListView.as_view(), name="receipt_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account_view, name="create_account"),
]
